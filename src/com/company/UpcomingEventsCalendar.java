package com.company;

import org.w3c.dom.events.EventException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UpcomingEventsCalendar {
    private LocalDate creationDate;
    private List<Event> eventList;

    public UpcomingEventsCalendar() {
        this.creationDate = LocalDate.now();
        this.eventList = new ArrayList<>();
    }

    public void addEvent(Event event) {
        if (event.getDate().isBefore(creationDate)) {
            throw new DateTimeException("This event is out of date.");
        }
        eventList.add(event);
    }

    public void showEventList() {
        for (int i = 0; i < eventList.size(); i++) {
            System.out.println(eventList.get(i));
        }
    }
}
