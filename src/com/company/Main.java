package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Event> allEvents = new ArrayList<>(Arrays.asList(
	    new Event("2020-12-31", "New Year's Eve"),
	    new Event("2020-01-15", "Random Day"),
	    new Event("2020-12-24", "Christmas Eve")));

	    UpcomingEventsCalendar upcomingEvents1 = new UpcomingEventsCalendar();
        allEvents.forEach(upcomingEvents1::addEvent);
        upcomingEvents1.showEventList();
        allEvents.add(new Event("2017-11-11", "Independence Day"));
        UpcomingEventsCalendar upcomingEvents2 = new UpcomingEventsCalendar();
        allEvents.forEach(upcomingEvents2::addEvent);
        // EventOutOfDateException: This even is out of date
    }
}
