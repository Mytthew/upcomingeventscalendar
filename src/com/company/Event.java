package com.company;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Event {
    private LocalDate date;
    private String eventName;
    DateTimeFormatter formatter = null;

    public Event(String d, String name){
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        date = LocalDate.parse(d, formatter);
        this.eventName = name;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Event: " +
                eventName + " => " + date;
    }
}
